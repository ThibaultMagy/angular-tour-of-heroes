import { Hero } from './hero';

export const HEROES: Hero[] = [
  { id: 11, name: 'Mr Jean' },
  { id: 12, name: 'PabloNarco' },
  { id: 13, name: 'Bosetti' },
  { id: 14, name: 'Captain Falcon' },
  { id: 15, name: 'VanDam' },
  { id: 16, name: 'ChuckN' },
  { id: 17, name: 'Dr PhilGood' },
  { id: 18, name: 'PedroMiguelPauleta' },
  { id: 19, name: 'HarryRoseLeMac' },
  { id: 20, name: 'Biglouche' }
];
